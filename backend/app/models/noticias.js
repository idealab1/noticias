"use strict";

const connection = require("../../config/connection");

var dataModels = {
  getNoticias: (callback) => {
    if (connection) {
      let sql = `SELECT * FROM noticias ORDER BY fecha DESC`;

      connection.query(sql, (error, rows) => {
        if (error) throw error;
        callback(rows);
      });
    }
  },
  getOneNoticia: (data, callback) => {
    console.log("el id : ", data);
    if (connection) {
      let sql = `SELECT * FROM noticias WHERE id_noticias = ${connection.escape(
        data
      )}`;

      connection.query(sql, (error, rows) => {
        if (error) throw error;
        callback(rows);
      });
    }
  },
  deleteNoticia: (data, callback) => {
    if (connection) {
      let sql = `DELETE FROM noticias WHERE id_noticias = ${connection.escape(
        data
      )}`;

      connection.query(sql, (error, rows) => {
        if (error) throw error;
        callback({ message: "noticia eliminada" });
      });
    }
  },
};

module.exports = dataModels;
