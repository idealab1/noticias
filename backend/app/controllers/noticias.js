"use strict";

const dataModels = require("../models/noticias");
const connection = require("../../config/connection");

async function getNoticias(req, res) {
  dataModels.getNoticias((data, error) => {
    res.json(data);
  });
}

function getOneNoticia(req, res) {
  const { id } = req.params;
  dataModels.getOneNoticia(id, (data, error) => {
    res.json(data);
  });
}

function deleteNoticia(req, res) {
  const { id } = req.params;
  dataModels.deleteNoticia(id, (data, error) => {
    res.json(data);
  });
}
module.exports = {
  getNoticias,
  getOneNoticia,
  deleteNoticia,
};
