"use strict";

const route = require("express").Router();
const {
  getNoticias,
  getOneNoticia,
  deleteNoticia,
} = require("../controllers/noticias");

route.route("/").get(getNoticias);

route.route("/:id").get(getOneNoticia);

route.route("/:id").delete(deleteNoticia);

module.exports = route;
